# Plataforma para Red Nacional de Centros de Innovación de Intel

Plataforma de Intel para mostrar los centros de innovación del país.

## Requisitos

* Python 3.6 (o superior)

## Instalar librerias necesarias
```
sudo apt-get install python-dev default-libmysqlclient-dev
sudo apt-get install python3-dev
```

## Instalar las dependencias del proyecto
```
pip install -r requirements.txt
```

## Crear la base de datos y un usuario en mysql
~~~~
mysql -u root -p
CREATE USER 'innovationlabs'@'localhost' IDENTIFIED BY 'innov4tionL4bs';
GRANT ALL PRIVILEGES ON * . * TO 'innovationlabs'@'localhost';
FLUSH PRIVILEGES;
create database db_innovationlabs;
~~~~

## Generar las Migraciones
```
python manage.py makemigrations
```

## Aplicar las Migraciones
```
python manage.py migrate
```

## Crear un Súper usuario
```
python manage.py createsuperuser
```

## Correr el proyecto
```
python manage.py runserver
```