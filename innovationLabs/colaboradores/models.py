from django.db import models
from stdimage import StdImageField, validators
from django.contrib.auth.models import User

class Colaborador(models.Model):
    laboratorio = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    nombre_laboratorio = models.CharField(max_length=200, blank=True, null=True)
    colaborador = models.CharField(verbose_name="colaborador", max_length=200)
    descripcion = models.CharField(verbose_name="descripcion", max_length=250, blank=True, null=True)
    sitioweb = models.CharField(verbose_name="sitioweb", max_length=250, blank=True, null=True)
    image = StdImageField(upload_to = "colaboradores", verbose_name="imagen", blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = "colaborador"
        verbose_name_plural = "colaboradores"
        ordering = ['created']

    def __str__(self):
        return self.title
