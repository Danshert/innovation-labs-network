from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Colaborador
from normativa.models import Normativa

def colaboradores(request):
	colaboradores = Colaborador.objects.all().order_by('-created')
	paginator = Paginator(colaboradores, 12)
	page = request.GET.get('page')

	normativas = Normativa.objects.all().order_by('created')

	try:
		colaboradores = paginator.page(page)
	except PageNotAnInteger:
		colaboradores = paginator.page(1)
	except EmptyPage:
		colaboradores = paginator.page(paginator.num_pages)
	return render(request, "colaboradores/colaboradores.html", {"colaboradores": colaboradores, 'normativas':normativas})
