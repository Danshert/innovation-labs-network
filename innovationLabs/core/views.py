from django.shortcuts import render
from django.http import HttpResponseRedirect
from noticias.models import Noticia
from normativa.models import Normativa
from laboratorios.models import Laboratory
from django.core import serializers

def home(request):
	noticias = Noticia.objects.filter(destacada=True).exclude(image='').order_by('-id')[:5]
	normativas = Normativa.objects.all().order_by('created')
	laboratorios = Laboratory.objects.all().order_by("?")
	labs = serializers.serialize('json', Laboratory.objects.all())
	
	return render(request, "core/home.html", {
		'noticias':noticias, 
		'normativas':normativas, 
		'laboratorios': laboratorios,
		'labs': labs
	})

def page_not_found (request, exception):
	return HttpResponseRedirect("/")