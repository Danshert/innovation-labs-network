from django.forms import ModelForm
from django import forms
from noticias.models import Noticia
from especializacion.models import Especializacion
from colaboradores.models import Colaborador
from recursos.models import Recurso
from django.forms.widgets import HiddenInput

class NoticiaForm(ModelForm):

    class Meta:
        model = Noticia
        fields = ['title', 'content', 'image', 'destacada']
        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control mb-2', 'placeholder':'Título de la Noticia'}),
            'image': forms.ClearableFileInput(attrs={'class':'mb-2'}),
            'destacada': forms.CheckboxInput,
        }

        labels = {
			'title':'Título', 
            'content': 'Contenido',
            'image': 'Imágen',
            'destacada': 'Destacada'
		}

class EspecializacionForm(ModelForm):

    class Meta:
        model = Especializacion
        fields = ['title', 'content', 'image']
        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control mb-2', 'placeholder':'Título de la Especialización'}),
            'image': forms.ClearableFileInput(attrs={'class':'mb-2'}),
        }

        labels = {
			'title':'Título', 
            'content': 'Contenido',
            'image': 'Imágen',
		}

class RecursoForm(ModelForm):

    class Meta:
        model = Recurso
        fields = ['title', 'archivo']
        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Nombre del Archivo'}),
            'archivo': forms.ClearableFileInput(attrs={'class':'mb-3'}),
        }

        labels = {
			'title':'Nombre', 
            'archivo': 'Archivo',
		}

class ColaboradorForm(ModelForm):

    class Meta:
        model = Colaborador
        fields = ['colaborador', 'descripcion', 'sitioweb', 'image']
        widgets = {
            'colaborador': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Nombre del Colaborador'}),
            'descripcion': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Descripción'}),
            'sitioweb': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Sitio Web'}),
            'image': forms.ClearableFileInput(attrs={'class':'mb-2'}),
        }

        labels = {
			'colaborador':'Colaborador', 
            'descripcion': 'Descripción',
            'sitioweb': 'Sitio Web',
            'image': 'Imágen',
		}