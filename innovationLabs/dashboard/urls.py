from django.urls import path
from .views import NoticiaListView, NoticiaCreate, NoticiaUpdate, NoticiaDelete 
from .views import EspecializacionListView, EspecializacionCreate, EspecializacionUpdate
from .views import EspecializacionDelete, RecursoCreate, RecursoListView, RecursoDelete, RecursoUpdate
from .views import ColaboradorListView, ColaboradorCreate, ColaboradorUpdate, ColaboradorDelete 
from . import views

app_name = "dashboard"

urlpatterns = [
	path('', views.dashboard, name="dashboard"),

	path('noticias-publicadas/', NoticiaListView.as_view(), name="publicadas"),
	path('nueva-noticia/', NoticiaCreate.as_view(), name="nueva"),
	path('editar-noticia/<int:pk>/', NoticiaUpdate.as_view(), name="editar"),
	path('borrar-noticia/<int:pk>/', NoticiaDelete.as_view(), name="borrar"),

	path('especializaciones-publicadas/', EspecializacionListView.as_view(), name="especializaciones-publicadas"),
	path('nueva-especializacion/', EspecializacionCreate.as_view(), name="nueva-especializacion"),
	path('editar-especializacion/<int:pk>/', EspecializacionUpdate.as_view(), name="editar-especializacion"),
	path('borrar-especializacion/<int:pk>/', EspecializacionDelete.as_view(), name="borrar-especializacion"),

	path('nuevo-recurso/', RecursoCreate.as_view(), name="nuevo-recurso"),
	path('recursos-publicados/', RecursoListView.as_view(), name="recursos-publicados"),
	path('borrar-recurso/<int:pk>/', RecursoDelete.as_view(), name="borrar-recurso"),
	path('editar-recurso/<int:pk>/', RecursoUpdate.as_view(), name="editar-recurso"),

	path('nuevo-colaborador/', ColaboradorCreate.as_view(), name="nuevo-colaborador"),
	path('colaboradores-publicados/', ColaboradorListView.as_view(), name="colaboradores-publicados"),
	path('borrar-colaborador/<int:pk>/', ColaboradorDelete.as_view(), name="borrar-colaborador"),
	path('editar-colaborador/<int:pk>/', ColaboradorUpdate.as_view(), name="editar-colaborador")

]