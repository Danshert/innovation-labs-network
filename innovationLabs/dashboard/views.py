from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy, reverse
from noticias.models import Noticia
from especializacion.models import Especializacion
from recursos.models import Recurso
from colaboradores.models import Colaborador
from .forms import NoticiaForm
from .forms import RecursoForm
from .forms import EspecializacionForm
from .forms import ColaboradorForm
from django import forms
from django.contrib.sessions.models import Session
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib import messages

def dashboard(request):
	if not request.user.is_authenticated:
		return HttpResponseRedirect("/")
		
	return render(request, "dashboard/dashboard_content.html")

# CRUD NOTICIAS

@method_decorator(login_required, name = 'dispatch')
class NoticiaListView(ListView):
	template_name = 'noticias/noticia_list.html'
	paginate_by = 4
	model = Noticia

	def get_queryset(self):
		return Noticia.objects.filter(laboratorio=self.request.user).order_by('-created')[:20]

@method_decorator(login_required, name = 'dispatch')
class NoticiaCreate(CreateView):
	template_name = 'noticias/noticia_form.html'
	form_class = NoticiaForm

	def form_valid(self, form):
		post = form.save(commit=False)
		post.laboratorio = self.request.user
		laboratorio = ""
		grupos_query = Group.objects.filter(user = self.request.user)
		for g in grupos_query:
			laboratorio = g.name
		post.nombre_laboratorio = laboratorio
		post.save()
		messages.success(self.request, 'Noticia creada correctamente')
		return HttpResponseRedirect(reverse('dashboard:publicadas'))

@method_decorator(login_required, name = 'dispatch')
class NoticiaUpdate(UpdateView):
	model = Noticia
	fields = ['title', 'content', 'image', 'destacada']

	def get_success_url(self):
		return reverse_lazy('dashboard:editar', args=[self.object.id]) + '?ok'

@method_decorator(login_required, name = 'dispatch')
class NoticiaDelete(DeleteView):
	model = Noticia

	def get_success_url(self):
		messages.success(self.request, 'Noticia eliminada correctamente')
		return reverse_lazy('dashboard:dashboard')

# CRUD ESPECIALIZACION

@method_decorator(login_required, name = 'dispatch')
class EspecializacionListView(ListView):
	template_name = 'especializacion/especializacion_list.html'
	paginate_by = 4
	model = Especializacion

	def get_queryset(self):
		return Especializacion.objects.filter(laboratorio=self.request.user).order_by('-created')[:50]

@method_decorator(login_required, name = 'dispatch')
class EspecializacionCreate(CreateView):
	template_name = 'especializacion/especializacion_form.html'
	form_class = EspecializacionForm

	def form_valid(self, form):
		post = form.save(commit=False)
		post.laboratorio = self.request.user
		laboratorio = ""
		grupos_query = Group.objects.filter(user = self.request.user)
		for g in grupos_query:
			laboratorio = g.name
		post.nombre_laboratorio = laboratorio
		post.save()
		messages.success(self.request, 'Especializacion creada correctamente')
		return HttpResponseRedirect(reverse('dashboard:especializaciones-publicadas'))

@method_decorator(login_required, name = 'dispatch')
class EspecializacionUpdate(UpdateView):
	model = Especializacion
	fields = ['title', 'content', 'image']
	
	def get_success_url(self):
		return reverse_lazy('dashboard:editar-especializacion', args=[self.object.id]) + '?ok'

@method_decorator(login_required, name = 'dispatch')
class EspecializacionDelete(DeleteView):
	model = Especializacion

	def get_success_url(self):
		messages.success(self.request, 'Especializacion eliminada correctamente')
		return reverse_lazy('dashboard:dashboard')

# CRUD RECURSOS

@method_decorator(login_required, name = 'dispatch')
class RecursoListView(ListView):
	template_name = 'noticias/recurso_list.html'
	model = Recurso
	paginate_by = 5
	
	def get_queryset(self):
		return Recurso.objects.filter(laboratorio=self.request.user).order_by('-created')[:20]

@method_decorator(login_required, name = 'dispatch')
class RecursoCreate(CreateView):
	template_name = 'recursos/recurso_form.html'
	form_class = RecursoForm

	def form_valid(self, form):
		post = form.save(commit=False)
		post.laboratorio = self.request.user
		laboratorio = ""
		grupos_query = Group.objects.filter(user = self.request.user)
		for g in grupos_query:
			laboratorio = g.name
		post.nombre_laboratorio = laboratorio
		post.save()
		messages.success(self.request, 'Recurso subido correctamente')
		return HttpResponseRedirect(reverse('dashboard:recursos-publicados'))

@method_decorator(login_required, name = 'dispatch')
class RecursoUpdate(UpdateView):
	model = Recurso
	fields = ['title', 'archivo']
	widgets = {
		'title': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Nombre del Archivo'}),
		'archivo': forms.ClearableFileInput(attrs={'class':'mb-3'}),
	}
	template_name_suffix = '_update_form'

	def get_success_url(self):
		return reverse_lazy('dashboard:editar-recurso', args=[self.object.id]) + '?ok'

@method_decorator(login_required, name = 'dispatch')
class RecursoDelete(DeleteView):
	model = Recurso

	def get_success_url(self):
		messages.success(self.request, 'Recurso eliminado correctamente')
		return reverse_lazy('dashboard:recursos-publicados')

# CRUD COLABORADORES

@method_decorator(login_required, name = 'dispatch')
class ColaboradorListView(ListView):
	template_name = 'colaboradores/colaborador_list.html'
	paginate_by = 4
	model = Colaborador

	def get_queryset(self):
		return Colaborador.objects.filter(laboratorio=self.request.user).order_by('-created')[:20]

@method_decorator(login_required, name = 'dispatch')
class ColaboradorCreate(CreateView):
	template_name = 'colaboradores/colaborador_form.html'
	form_class = ColaboradorForm

	def form_valid(self, form):
		post = form.save(commit=False)
		post.laboratorio = self.request.user
		laboratorio = ""
		grupos_query = Group.objects.filter(user = self.request.user)
		for g in grupos_query:
			laboratorio = g.name
		post.nombre_laboratorio = laboratorio
		post.save()
		messages.success(self.request, 'Colaborador creado correctamente')
		return HttpResponseRedirect(reverse('dashboard:colaboradores-publicados'))

@method_decorator(login_required, name = 'dispatch')
class ColaboradorUpdate(UpdateView):
	model = Colaborador
	fields = ['colaborador', 'descripcion', 'sitioweb', 'image']
	widgets = {
            'colaborador': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Nombre del Colaborador'}),
            'descripcion': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Descripción'}),
            'sitioweb': forms.TextInput(attrs={'class':'form-control mb-3', 'placeholder':'Sitio Web'}),
            'image': forms.ClearableFileInput(attrs={'class':'mb-2'}),
        }
	labels = {
			'colaborador':'Colaborador', 
            'descripcion': 'Descripción',
            'sitioweb': 'Sitio Web',
            'image': 'Imágen',
		}
	template_name_suffix = '_update_form'
	
	def get_success_url(self):
		return reverse_lazy('dashboard:editar-colaborador', args=[self.object.id]) + '?ok'

@method_decorator(login_required, name = 'dispatch')
class ColaboradorDelete(DeleteView):
	model = Colaborador

	def get_success_url(self):
		messages.success(self.request, 'Colaborador eliminado correctamente')
		return reverse_lazy('dashboard:dashboard')