from django.apps import AppConfig


class EspecializacionConfig(AppConfig):
    name = 'especializacion'
