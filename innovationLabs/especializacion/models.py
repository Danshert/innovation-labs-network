from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from stdimage import StdImageField, validators
# from stdimage.validators import MinSizeValidator, MaxSizeValidator

class Especializacion(models.Model):
    laboratorio = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    nombre_laboratorio = models.CharField(max_length=200, blank=True, null=True)
    title = models.CharField(verbose_name="titulo", max_length=200)
    content = RichTextField(verbose_name="contenido")
    # image = StdImageField(upload_to = "noticiasEspecializacion", verbose_name="imagen", blank=True, null=True, variations={'thumbnail': (960, 540)}, validators=[MinSizeValidator(960, 540)])
    image = StdImageField(upload_to = "noticiasEspecializacion", verbose_name="imagen", blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Especialización"
        verbose_name_plural = "Especializaciones"
        ordering = ['created']

    def __str__(self):
        return self.title