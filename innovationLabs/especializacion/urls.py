from django.urls import path
from . import views

urlpatterns = [
	path('', views.especializaciones, name="especializacion"),
	path('especializacion/<int:especializacion_id>/<slug:especializacion_slug>/', views.especializacion, name="especializacion"),
]