from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import Group, User
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Especializacion
from normativa.models import Normativa

def especializaciones(request):

	especializaciones = Especializacion.objects.all().order_by('-created')[:36]
	normativas = Normativa.objects.all().order_by('created')

	paginator = Paginator(especializaciones, 9)
	page = request.GET.get('page')

	try:
		especializaciones = paginator.page(page)
	except PageNotAnInteger:
		especializaciones = paginator.page(1)
	except EmptyPage:
		especializaciones = paginator.page(paginator.num_pages)

	return render(request, "especializacion/especializaciones.html", {'especializaciones': especializaciones, 'normativas':normativas})


def especializacion(request, especializacion_id, especializacion_slug):
	especializacion = get_object_or_404(Especializacion, id=especializacion_id)
	normativas = Normativa.objects.all().order_by('created')
	publicaciones_recientes = Especializacion.objects.exclude(id=especializacion_id).order_by('-id')[:4]

	try:
		querry_group=Group.objects.filter(user=especializacion.laboratorio_id)
	except:
		querry_group=None
	
	return render(request, "especializacion/especializacion.html", {
		'especializacion': especializacion,
		'publicaciones_recientes': publicaciones_recientes,
		'groups': querry_group, 
		'normativas':normativas
	})
