"""innovationLabs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import handler404, handler500
from django.conf.urls.static import static
from django.conf import settings

handler404 = 'core.views.page_not_found'

urlpatterns = [
	# paths del core
	path('', include('core.urls')),
    # paths de noticias
    path('noticias/', include('noticias.urls')),
    # paths de especializacion
    path('especializacion/', include('especializacion.urls')),
    # paths de normativa
    path('normativas/', include('normativa.urls')),
    # paths de laboratorios
    path('laboratorios/', include('laboratorios.urls')),
    # paths de recursos
    path('recursos/', include('recursos.urls')),
    # paths de colaboradores
    path('colaboradores/', include('colaboradores.urls')),
    # paths de login
    path('accounts/', include('django.contrib.auth.urls')),
    # paths de dashboard
    path('dashboard/', include('dashboard.urls')),
	# paths del admin
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
