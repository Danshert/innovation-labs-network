from django.contrib import admin
from .models import Laboratory

# Register your models here.
class LaboratoriesAdmin(admin.ModelAdmin):
    pass
        
admin.site.register(Laboratory, LaboratoriesAdmin)