from django.db import models
from stdimage import StdImageField, validators
from ckeditor.fields import RichTextField

class Laboratory(models.Model):
    nombre = models.CharField(max_length=100, verbose_name="Nombre del laboratorio")
    abreviatura = models.CharField(max_length=30, verbose_name="Abreviatura del laboratorio")
    descripcion = RichTextField(verbose_name="Descripción del laboratorio")
    direccion = models.CharField(max_length=400, verbose_name="Dirección del laboratorio (Calle, Colonia, Número, CP, Municipio, Entidad.)")
    telefonos = models.CharField(max_length=100, verbose_name="Teléfonos")
    pagina_web = models.CharField(max_length=200, verbose_name="Sitio Web", blank=True, null=True)
    # latitud = models.CharField(max_length=50, verbose_name="Latitud")
    # longitud = models.CharField(max_length=50, verbose_name="Longitud")
    logotipo = StdImageField(upload_to = "laboratorios/logotipos", verbose_name="Logotipo")
    imagen_portada = StdImageField(upload_to = "laboratorios", verbose_name="Imágen de Portada")
    imagen_2 = StdImageField(upload_to = "laboratorios", verbose_name="Imágen 2", blank=True, null=True)
    imagen_3 = StdImageField(upload_to = "laboratorios", verbose_name="Imágen 3", blank=True, null=True)
    imagen_4 = StdImageField(upload_to = "laboratorios", verbose_name="Imágen 4", blank=True, null=True)

    class Meta:
        verbose_name = "Laboratorio"
        verbose_name_plural = "Laboratorios"

    def __str__(self):
        return self.abreviatura + " - " + self.nombre