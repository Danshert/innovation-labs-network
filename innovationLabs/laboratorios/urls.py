from django.urls import path
from . import views

urlpatterns = [
	path('', views.laboratorios, name="laboratorios"),
	path('laboratorio/<int:laboratorio_id>/<slug:laboratorio_slug>/', views.laboratorio, name="laboratorio"),
]
