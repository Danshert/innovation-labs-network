from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from .models import Laboratory
from normativa.models import Normativa

def laboratorios(request):
	laboratorios = Laboratory.objects.all().order_by('nombre')
	normativas = Normativa.objects.all().order_by('created')
	return render(request, "laboratorios/laboratorios.html", {'laboratorios': laboratorios, 'normativas':normativas})

def laboratorio(request, laboratorio_id, laboratorio_slug):
	laboratorio = get_object_or_404(Laboratory, id=laboratorio_id)
	normativas = Normativa.objects.all().order_by('created')
	return render(request, "laboratorios/laboratorio.html", {'laboratorio': laboratorio, 'normativas':normativas})