from django.contrib import admin
from .models import Normativa

class NormativasAdmin(admin.ModelAdmin):
    pass

admin.site.register(Normativa, NormativasAdmin)
