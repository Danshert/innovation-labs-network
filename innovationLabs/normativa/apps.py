from django.apps import AppConfig


class NormativaConfig(AppConfig):
    name = 'normativa'
