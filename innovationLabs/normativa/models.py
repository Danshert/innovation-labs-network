from django.db import models
from ckeditor.fields import RichTextField

class Normativa(models.Model):
    title = models.CharField(verbose_name="titulo", max_length=200)
    content = RichTextField(verbose_name="contenido")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = "normativa"
        verbose_name_plural = "normativas"
        ordering = ['created']

    def __str__(self):
        return self.title