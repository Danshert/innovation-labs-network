from django.urls import path
from . import views

urlpatterns = [
	path('normativa/<int:normativa_id>/<slug:normativa_slug>/', views.normativa, name="normativa"),
]
