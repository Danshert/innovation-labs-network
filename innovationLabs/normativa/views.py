from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import Group, User
from normativa.models import Normativa

def normativa(request, normativa_id, normativa_slug):
	normativa = get_object_or_404(Normativa, id=normativa_id)
	normativas = Normativa.objects.all().order_by('created')

	try:
		querry_group=Group.objects.filter(user=normativa.laboratorio_id)
	except:
		querry_group=None

	return render(request, "normativa/normativa.html", {'normativa': normativa, 'groups': querry_group, 'normativas':normativas})
