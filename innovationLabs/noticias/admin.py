from django.contrib import admin
from .models import Noticia

class NoticiasAdmin(admin.ModelAdmin):
    pass

admin.site.register(Noticia, NoticiasAdmin)
