from django.urls import path
from . import views

urlpatterns = [
	path('', views.noticias, name="noticias"),
	path('noticia/<int:noticia_id>/<slug:noticia_slug>/', views.noticia, name="noticia"),
]