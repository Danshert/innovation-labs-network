from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import Group, User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Noticia
from normativa.models import Normativa

def noticias(request):
	noticias = Noticia.objects.all().order_by('-created')[:45]
	paginator = Paginator(noticias, 9)
	page = request.GET.get('page')

	normativas = Normativa.objects.all().order_by('created')

	try:
		noticias = paginator.page(page)
	except PageNotAnInteger:
		noticias = paginator.page(1)
	except EmptyPage:
		noticias = paginator.page(paginator.num_pages)
	return render(request, "noticias/noticias.html", {'noticias': noticias, 'normativas':normativas})

def noticia(request, noticia_id, noticia_slug):
	noticia = get_object_or_404(Noticia, id=noticia_id)
	noticias_recientes = Noticia.objects.exclude(id=noticia_id).order_by('-id')[:4]
	normativas = Normativa.objects.all().order_by('created')

	try:
		querry_group=Group.objects.filter(user=noticia.laboratorio_id)
	except:
		querry_group=None

	return render(request, "noticias/noticia.html", {
		'noticia': noticia, 
		'noticias_recientes': noticias_recientes, 
		'groups': querry_group, 
		'normativas':normativas
	})
