from django.contrib import admin
from .models import Recurso

class RecursosAdmin(admin.ModelAdmin):
    pass

admin.site.register(Recurso, RecursosAdmin)
