from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
import os

class Recurso(models.Model):
    laboratorio = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    nombre_laboratorio = models.CharField(max_length=200, blank=True, null=True)
    title = models.CharField(verbose_name="titulo", max_length=200)
    content = RichTextField(verbose_name="contenido", blank=True, null=True)
    archivo = models.FileField(verbose_name="archivo", upload_to="recursos")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = "archivo"
        verbose_name_plural = "archivos"
        ordering = ['created']

    def __str__(self):
        return os.path.basename(self.archivo.name)     