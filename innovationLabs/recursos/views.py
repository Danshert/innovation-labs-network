from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Recurso
from normativa.models import Normativa

def recursos(request):
	recursos = Recurso.objects.all().order_by('-created')[:50]
	paginator = Paginator(recursos, 10)
	page = request.GET.get('page')

	normativas = Normativa.objects.all().order_by('created')

	try:
		recursos = paginator.page(page)
	except PageNotAnInteger:
		recursos = paginator.page(1)
	except EmptyPage:
		recursos = paginator.page(paginator.num_pages)
	return render(request, "recursos/recursos.html", {"recursos": recursos, 'normativas':normativas})
